FROM ligo/builder:el7

LABEL name="BayesWave LALInference_o2 EL7" \
      maintainer="James Alexander CLark <james.clark@ligo.org>" \
      date="20190328" \
      support="Experimental Platform"

# Setup directories for binding
RUN mkdir -p /cvmfs /hdfs /hadoop /etc/condor

# Dependencies
RUN yum upgrade -y && \
    yum clean all && \
    rm -rf /var/cache/yum

RUN yum update -y && \
      yum install -y lscsoft-lalsuite-dev cmake3 && \
      yum clean all && \
      rm -rf /var/cache/yum

# Clone and build lalsuite from source
RUN git clone https://git.ligo.org/lscsoft/lalsuite.git
RUN cd lalsuite && \
      ./00boot  && \
      ./configure --prefix /opt/lscsoft/lalsuite --enable-swig-python --disable-lalstochastic --disable-laldetchar && \
      make -j && \
      make install && \
      cd / && \
      rm -rf lalsuite
      #git checkout -b lalinference_o2 origin/lalinference_o2 && \

# Copy files from context into container and build BayesWave
COPY .git /.git
COPY install.sh /install.sh
COPY build.sh /build.sh
COPY src /src
COPY CMakeLists.txt /CMakeLists.txt
COPY bayeswave.spec.in /bayeswave.spec.in
COPY etc/bayeswave-user-env.sh /etc/bayeswave-user-env.sh
COPY BayesWaveUtils /BayesWaveUtils
RUN ["/bin/bash", "-c", "source /opt/lscsoft/lalsuite/etc/lalsuiterc && \
      sh install.sh /opt/lscsoft/bayeswave"]
RUN rm -rf /.git /install.sh /build.sh /src /bayeswave.spec.in /BayesWaveUtils /etc/bayeswave-user-env.sh

# Set up an entry point so we can source lalsuite and bayeswave user env scripts at startup
COPY docker-entrypoint.sh /usr/local/bin/
RUN ln -s usr/local/bin/docker-entrypoint.sh / # backwards compat
ENTRYPOINT ["docker-entrypoint.sh"]

# Keep a note of explicit build commmand for potential future use:
# RUN ["/bin/bash", "-c", "source /opt/lscsoft/lalsuite/etc/lalsuiterc && \
#       mkdir build && \
#       pushd build && \
#       cmake3 .. \
#         -DCMAKE_INSTALL_PREFIX=/opt/lscsoft/bayeswave \
#         -DCMAKE_BUILD_TYPE=DEBUG \
#         -DCMAKE_EXPORT_COMPILE_COMMANDS=true && \
#       cmake3 --build . -- VERBOSE=1 && \
#       cmake3 --build . --target install"]
